from unittest import TestCase
from microclient.api import *
import responses


API = {
    "foo": {
        "base_url": "https://foo.com",
        "resources": {
            "bar": {
                "path": "foo/bar/baz/"
            }
        }
    },
}


class MockMicro:

    def __init__(self, micro):
        self.micro = micro

    def expect(self, resource, id=None, response_data={}, response_status=200):
        url = self.micro._prepare_request(resource, id)
        responses.add(
            responses.GET,
            url,
            json=response_data,
            status=response_status
        )


class MicroAPITestCase(TestCase):

    def setUp(self):
        self.micro = Micro(API, "foo")
        self.mock_micro = MockMicro(self.micro)

    def test_get_micro(self):
        m = get_micro("foo", 1)
        assert m.headers.get('X_AUTHENTICATED_USERID') == '1'
        assert isinstance(m ,Micro)

    @responses.activate
    def test_can_list(self):
        self.mock_micro.expect("bar")
        result, data, is_ok = self.micro.list("bar")

    @responses.activate
    def test_can_get(self):
        self.mock_micro.expect("bar", 1)
        result, data, is_ok = self.micro.get("bar", 1)