import responses
import requests


def get_micro(service, user_id, consumer_username="", api_config={}):
    """
    micro = get_micro("appointmentguru", 1)
    """
    headers = {
        'X_ANONYMOUS_CONSUMER': 'False',
        'X_AUTHENTICATED_USERID': str(user_id),
        'X_CONSUMER_USERNAME': consumer_username,
    }
    return Micro(api_config, service, headers=headers)


class Micro:
    '''
    usage: response, data, is_ok = Micro(API, 'appointmentguru', id).list('appointment', default={})
    usage: response, data, is_ok = Micro(API, 'appointmentguru', id).get('appointment', id, default={})
    '''

    def __init__(self, api_definition, service_name, headers={}):
        self.defn = api_definition
        self.service = service_name
        self.headers = headers

    def _prepare_request(self, resource_name, id = None):
        defn = self.defn.get(self.service)
        base_url = defn.get('base_url')
        path = defn.get('resources', {}).get(resource_name).get('path')
        if id is not None:
            path = "{}{}/".format(path, id)

        return "{}/{}".format(base_url, path)

    def __make_request(self, url, params=None, extra_headers={}):
        headers = self.headers.copy()
        headers.update(extra_headers)
        data = {
            "headers": headers
        }
        return requests.get(url, **data)

    def __get_response_data(self, response, default_response={}):
        if response.ok:
            return response.json()
        else:
            return default_response

    def list(self, resource, params=None, extra_headers={}, default_response={}):
        url = self._prepare_request(resource)
        response = self.__make_request(url, params, extra_headers)
        data = self.__get_response_data(response)
        return (response, data, response.ok)

    def get(self, resource, id, params=None, extra_headers={}, default_response={}):
        url = self._prepare_request(resource, id)
        response = self.__make_request(url, params, extra_headers)
        data = self.__get_response_data(response)
        return (response, data, response.ok)
